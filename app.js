import fastify from "fastify";

export const createApp = (opts = {}) => {
  const app = fastify(opts);

  app.get("/", function (req, rep) {
    return { hello: "WORLD" };
  });

  return app;
};
