"use strict";

import { createApp } from "./app.js";
import { test } from "tap";

test('requests the "/" route', async (t) => {
  const app = createApp();

  const response = await app.inject({ method: "GET", url: "/" });

  t.equal(response.statusCode, 200, "returns a status code of 200");
});
