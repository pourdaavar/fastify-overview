import { createApp } from "./app.js";

const server = createApp({
  logger: {
    level: "info",
    transport: {
      target: "pino-pretty",
    },
  },
});

/**
 * Run the server!
 */
const bootstrap = async () => {
  try {
    await server.listen({ port: 3000, host: "localhost" });
  } catch (err) {
    server.log.error(err);
    process.exit(1);
  }
};
void bootstrap();
